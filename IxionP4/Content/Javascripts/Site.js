﻿
$("#show-features").on("click", function () {
    $(".container-wrap-3")
        .removeAttr("id")
        .hide();
    $(".container-wrap-4")
        .attr("id", "features")
        .show();

    $(window).scrollTo("#features", 200);
});
$("#hide-features").on("click", function () {
    $(".container-wrap-4")
        .removeAttr("id")
        .hide();
    $(".container-wrap-3")
        .attr("id", "features")
        .show();
});
$(".navbar-nav li").on("click", function () {
    $(".navbar-nav .active").removeClass("active");
    $(this).addClass("active");
});
$(".navbar-nav a, .navbar-brand").on("click", function (event) {
    var idEl = this.href.split("#")[1];
    if (idEl == "preview" || idEl == "features")
        $(window).scrollTo("#" + idEl, 500, {offset: 80});
    else 
        $(window).scrollTo("#" + idEl, 500);


    event.preventDefault();
});
$(".navbar-btn").on("click", function (event) {
    $(window).scrollTo("#buy", 500);
    event.preventDefault();
});
$(".navbar-brand").on("click", function () {
    $(".navbar-nav .active").removeClass("active");
});
var Gallery = (function ($) {
    function Gallery() {
        this.init.apply(this, arguments);
    }

    $.extend(Gallery.prototype, {
        defaults: {
            images: []
            , current: 0
            , selector: ".container-wrap-5 a[data-index]"
        }
        , init: function (options) {
            $.extend(this, this.defaults, options);
            this.parseImages();
            this.preloadImages();

            var galleryObj = this;
            $(this.selector).on("click", function (event) {
                event.preventDefault();

                var startIndex = $(this).data("index");
                
                galleryObj.showGallery(startIndex, event.pageX, event.pageY - $(window).scrollTop());
            });
        }
        , parseImages: function () {
            if (this.images.length > 0)
                return;

            var galleryObj = this;
            $(this.selector).each(function (i) {
                var dataIndex = $(this).data("index");
                if (dataIndex === undefined)
                    dataIndex = i;
                if (galleryObj.images[dataIndex] == undefined)
                    galleryObj.images[dataIndex] = this.href;
            });
        }
        , preloadImages: function () {
            for (var i in this.images) {
                new Image().src = this.images[i];
            }
        }
        , showGallery: function (startIndex, x, y) {
            this.current = startIndex;
            // удалим старую галерею
            $(".gallery-frame").remove();
            $('<div class="gallery-frame" style="width: 0; height: 0; top: ' + y + 'px; left: ' + x + 'px;">' +
                '<table>' +
                    '<tr>' +
                        '<td>' +
                            '<img class="gallery-image" src="' + this.images[startIndex] + '" alt="" />' +
                            '<span class="gallery-prev"></span>' +
                            '<span class="gallery-next"></span>' +
                            '<span class="gallery-close">' +
                                '<i class="fa fa-times"></i>' +
                            '</span>' +
                        '</td>' +
                    '</tr>' +
                '</table>' +
            '</div>')
                .prependTo("body")
                .animate({
                    width: "100%"
                    , height: "100%"
                    , top: 0
                    , left: 0
                }
                , {
                    duration: 200
                    , complete: function () {
                        // уберем скрол с body
                        $("body").css({
                            width: 100
                            , height: 100
                            , overflow: "hidden"
                        });
                    }
                });

            $(".gallery-prev").on("click", $.proxy(this.prev, this));
            $(".gallery-next").on("click", $.proxy(this.next, this));
            $(".gallery-close").on("click", $.proxy(this.close, this));
        }
        , next: function () {
            this.current++;
            if (this.current >= this.images.length)
                this.current = 0;

            if (this.images[this.current] === undefined) {
                this.next();
                return;
            }

            $(".gallery-image").attr("src", this.images[this.current]);
        }
        , prev: function () {
            this.current--;
            if (this.current <= 0)
                this.current = this.images.length - 1;

            if (this.images[this.current] === undefined) {
                this.prev();
                return;
            }

            $(".gallery-image").attr("src", this.images[this.current]);
        }
        , close: function () {
            $(".gallery-frame").remove();
            $("body").css({
                width: "auto"
                , height: "auto"
                , overflow: "visible"
            });
        }
    });
    return Gallery;
}($));

new Gallery();
///
var images = [
    "/Content/Styles/Images/Preview/1930x1080_.jpg"
    , "/Content/Styles/Images/Preview/1199x850_.jpg"
    , "/Content/Styles/Images/Preview/991x850_.jpg"
    , "/Content/Styles/Images/Preview/768x850_.jpg"
    , "/Content/Styles/Images/Service/1930x1080.jpg"
    , "/Content/Styles/Images/Service/1199x850.jpg"
    , "/Content/Styles/Images/Service/991x850.jpg"
    , "/Content/Styles/Images/Service/768x1200.jpg"
    , "/Content/Styles/Images/Service/780x461.png"
    , "/Content/Styles/Images/Service/646x382.png"
    , "/Content/Styles/Images/Service/652x370.png"
    , "/Content/Styles/Images/Service/415x245.png"
    , "/Content/Styles/Images/Buy/1930x1080.jpg"
    , "/Content/Styles/Images/Buy/1199x850.jpg"
    , "/Content/Styles/Images/Buy/991x850.jpg"
    , "/Content/Styles/Images/Buy/768x1200.jpg"
];
for (var i in images) {
    new Image().src = images[i];
}
// smooth scroll
if ($('html').hasClass('desktop')) {
    $.srSmoothscroll({
        step: 100,
        speed: 500
    });
}
// scrollorama          
function initScrollorama(
    c1Height, c2Height, c3Height, c4Height, c5Height, 
    bg1PartSel, bg2PartSel, bg3PartSel, bg4PartSel, bg5PartSel,
    duration
    ) {
    var delta = duration;
    var c1Top = 0;
    var c2Top = c1Top + c1Height;
    var c3Top = c2Top + c2Height;
    var c4Top = c3Top + c3Height;
    var c5Top = c4Top + c4Height;
    var bg1FadeOutEl = $(bg1PartSel + "-fadeout");
    var bg2FadeInEl = $(bg2PartSel + "-fadein");
    var bg2FadeOutEl = $(bg2PartSel + "-fadeout");
    var bg3FadeInEl = $(bg3PartSel + "-fadein");
    var bg3FadeOutEl = $(bg3PartSel + "-fadeout");
    var bg4FadeInEl = $(bg4PartSel + "-fadein");
    var bg4FadeOutEl = $(bg4PartSel + "-fadeout");
    var bg5FadeInEl = $(bg5PartSel + "-fadein");
    var bg5FadeOutEl = $(bg5PartSel + "-fadeout");

    winEl.on("scroll", function () {
        if (winEl.scrollTop() > c2Top) {
            bg2FadeInEl.css("display", "block");
            bg2FadeOutEl.css("display", "none");
        }
        if (winEl.scrollTop() > (c2Top + (c2Height / 4))) {
            bg2FadeInEl.css("display", "none");
            bg2FadeOutEl.css("display", "block");
        }
        //
        if (winEl.scrollTop() > c3Top) {
            bg3FadeInEl.css("display", "block");
            bg3FadeOutEl.css("display", "none");
        }
        if (winEl.scrollTop() > (c3Top + (c3Height / 4))) {
            bg3FadeInEl.css("display", "none");
            bg3FadeOutEl.css("display", "block");
        }
        //
        if (winEl.scrollTop() > c4Top) {
            bg4FadeInEl.css("display", "block");
            bg4FadeOutEl.css("display", "none");
        }
        if (winEl.scrollTop() > (c4Top + (c4Height / 4))) {
            bg4FadeInEl.css("display", "none");
            bg4FadeOutEl.css("display", "block");
        }
    });

    

    scrollorama.animate(bg1FadeOutEl, {
        delay: c1Top + (c1Height - 2 * duration),
        duration: duration,
        property: 'opacity',
        start: 1,
        end: 0
    });
    scrollorama.animate(bg2FadeInEl, {
        delay: c1Top + (c1Height - duration) - delta,
        duration: duration + delta,
        property: 'opacity',
        start: 0,
        end: 1
    });
    scrollorama.animate(bg2FadeOutEl, {
        delay: c2Top + (c2Height - 2 * duration),
        duration: duration,
        property: 'opacity',
        start: 1,
        end: 0
    });
    scrollorama.animate(bg3FadeInEl, {
        delay: c2Top + (c2Height - duration) - delta,
        duration: duration + delta,
        property: 'opacity',
        start: 0,
        end: 1
    });
    scrollorama.animate(bg3FadeOutEl, {
        delay: c3Top + (c3Height - 2 * duration),
        duration: duration,
        property: 'opacity',
        start: 1,
        end: 0
    });
    scrollorama.animate(bg4FadeInEl, {
        delay: c3Top + (c3Height - duration) - delta,
        duration: duration + delta,
        property: 'opacity',
        start: 0,
        end: 1
    });
    scrollorama.animate(bg4FadeOutEl, {
        delay: c4Top + (c4Height - 2 * duration),
        duration: duration,
        property: 'opacity',
        start: 1,
        end: 0
    });
    scrollorama.animate(bg5FadeInEl, {
        delay: c4Top + (c4Height - duration) - delta,
        duration: duration + delta,
        property: 'opacity',
        start: 0,
        end: 1
    });
}
//

var scrollorama = $.scrollorama({
    blocks: '.scrollblock'
});
var winEl = $(window);

var cHeight = 1080;
var c1Height, c2Height, c3Height, c4Height, c5Height;
c1Height = c2Height = c3Height = c4Height = c5Height = cHeight;
c3Height += 1760;
var duration = 300;
initScrollorama(
    c1Height, c2Height, c3Height, c4Height, c5Height, 
    ".bg-lg-1", ".bg-lg-2", ".bg-lg-3", ".bg-lg-4", ".bg-lg-5",
    duration
    );
var cHeight = 850;
var c1Height, c2Height, c3Height, c4Height, c5Height;
c1Height = c3Height = c4Height = c5Height = cHeight;
c2Height = 850;
c3Height += 1480;
var duration = 300;
initScrollorama(
    c1Height, c2Height, c3Height, c4Height, c5Height,
    ".bg-md-1", ".bg-md-2", ".bg-md-3", ".bg-md-4", ".bg-md-5",
    duration
    );
var cHeight = 850;
var c1Height, c2Height, c3Height, c4Height, c5Height;
c1Height = c2Height = c3Height = c4Height = c5Height = cHeight;
c3Height += 1160;
var duration = 300;
initScrollorama(
    c1Height, c2Height, c3Height, c4Height, c5Height,
    ".bg-sm-1", ".bg-sm-2", ".bg-sm-3", ".bg-sm-4", ".bg-sm-5",
    duration
    );
var cHeight = 1200;
var c1Height, c2Height, c3Height, c4Height, c5Height;
c1Height = c3Height = c4Height = c5Height = cHeight;
cHeight = 850;
c3Height += 2100;
var duration = 200;
initScrollorama(
    c1Height, c2Height, c3Height, c4Height, c5Height,
    ".bg-xs-1", ".bg-xs-2", ".bg-xs-3", ".bg-xs-4", ".bg-xs-5",
    duration
    );
var buyEl = $("#buy");
var deltaHeightEl = $(".container-wrap-8");
function adjustDeltaHeight() {
    var height = 0;
    if (winEl.height() > buyEl.height())
        height = winEl.height() - buyEl.height();
    if (height < 80)
        height = 80;
    deltaHeightEl.height(height);
}
winEl.on("resize", function () {
    adjustDeltaHeight();
});
adjustDeltaHeight();
// -- -- --
